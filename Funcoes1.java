import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
public class Funcoes1 {
    private ArrayList <Palindromo2> listaPalavra;
    public Funcoes1(){
        listaPalavra = new ArrayList();
    }

    public void addPalindrome(Palindromo2 pali){
        listaPalavra.add(pali);
    }

    public String mostrarPalindromo(String palindromo){
        Palindromo2 pali;

        for(int j = 0; j < listaPalavra.size(); j++){
            String contrario = "";
            for (int i = (palindromo.length() -1); i >= 0; i--) {
                contrario = contrario.trim() + palindromo.trim().charAt(i);
            }
            palindromo = palindromo.replace(" ","");
            if(contrario.equals(palindromo)){
                System.out.println(palindromo + "É palindromo" );
            }else{
                System.out.println(palindromo + "não é um palíndromo");
            }

        }
        return palindromo;
    }

    public static void main(String[] args){
        Funcoes1 pali = new Funcoes1();
        Scanner teclado = new Scanner(System.in);
        int opcao = 0;
        String palavra = "";
        while(opcao !=9){
            System.out. println("1 - Add palavras Palindromo");
            System.out. println("2 - mostrar palindromo ");
            System.out. println("9 - fim");
            opcao = teclado.nextInt();
            teclado.nextLine();
            switch(opcao){
                case 1: System.out. println("palavras:");
                    palavra = teclado.nextLine();
                    pali.addPalindrome(new Palindromo2(palavra));
                    break;
                case 2: System.out. println("mostrar os palindromos ");
                    System.out.println(pali.mostrarPalindromo(palavra));
                    break;
            }
        }
    }
}



