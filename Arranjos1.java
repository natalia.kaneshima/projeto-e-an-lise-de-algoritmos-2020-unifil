import java.util.Scanner;
public class Arranjos1 {
    public int arranjoSoma(int arr[]){
        int soma = 0;
        for (int i = 0; i < arr.length; i++){
            soma = soma + arr[i];
        }
        return soma;
    }

    public static void main(String [] args){
        Arranjos1 arr = new Arranjos1();
        int arrA[] = {3,3,7,8,9};
        System.out.println(arr.arranjoSoma(arrA));
    }

}
