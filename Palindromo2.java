import  java.util.ArrayList;
public class Palindromo2 {
    private String palavraPali;

    public Palindromo2(String palavraPali){
        this.palavraPali = palavraPali;
    }

    public String getPalavraPali(){
        return this.palavraPali;
    }

    public String palindrome(String palavra){
        String contrario = "";
        for (int i = (palavra.length() -1); i >= 0; i--) {
            contrario = contrario.trim() + palavra.trim().charAt(i);
        }

        palavra = palavra.replace(" ","");
        if(contrario.equals(palavra)){
            System.out.println("é um palíndromo");
        }else{
            System.out.println("não é um palíndromo");;
        }

        return palavra;
    }

    public static void main(String[] args){
        Palindromo2 pali = new Palindromo2("");
        System.out.println(pali.palindrome("roma me tem amor"));
    }
}
